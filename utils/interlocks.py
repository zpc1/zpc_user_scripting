from getter_functions import get_external_trap_status
###########################################################################################
external_trap_reason = '"External Trap" is not open'

# The functions should be used in a way that "1" means we have an interlock
manual_interlocks = {
    'V7': [external_trap_reason, not get_external_trap_status()[0]],
    'V8': [external_trap_reason, not get_external_trap_status()[0]],
}
###########################################################################################
def handle_manual_interlocks(channel):
    if channel in manual_interlocks:
        massage, interlock = manual_interlocks[channel]
        if interlock==1:# check if we have an interlock
            return massage
    return 'NA'
###########################################################################################