from env import WIRING_V

###########################################################################################
settings_map = {
    # Chamber Roughing
    "Roughing Max Time": ["OVCRoughDTSP", "floats"],
    "Roughing Setpoint": ["OVCRoughingSP", "floats"],
    "Vac. Max Time": ["OVCVacDTSP", "floats"],
    "Vac. Setpoint": ["OVCVacSP", "floats"],
    # Condensing
    "Condense Start": ["CondSPStartTemp", "floats"],
    "S1 Cycle High": ["CondInjSPScrollHigh", "floats"],
    "S1 Cycle Low": ["CondInjSPScrollLow", "floats"],
    "Dump Low": ["CondInjSPDumpLow", "floats"],
    "Dump Empty": ["CondInjSPDumpEmpty", "floats"],
    "Switching Delay": ["CondInjDelayDTSP", "int32s"],
    # Collecting Mix
    "Dump Full Setpoint": ["DumpFullPressSP", "floats"],
    "Dump Full Margin": ["DumpFullMargin", "floats"],
    # System
    "PLC Scan Delay": ["ChartScanDelay", "int32s"],
    "372 Scan Delay": ["LSCommsDelay", "int32s"],
    "Dump Pressure (Full)": ["P5DumpST", "floats"],
    # 'XXXXX': ['T1MaxPress', 'floats'],
    "T1 Start": ["T1StartPressSP", "floats"],
    "T1 Scan Delay": ["T1CommDelay", "int32s"],
    "T2 Ramp-Down": ["T2DecelDTSP", "int32s"],
    "T2 Scan Time": ["T2SerDTSP", "int32s"],
    # Pulsing
    "Pulsing Mix out": ["ACDP_mix_out_SP", "int32s"],
    "Pulsing Mix in": ["ACDP_mix_in_SP", "int32s"],
    "Pulsing Cycles": ["ResetDefaultSettings", "int32s"],
    #
    "Reset Defaults": ["ResetDefaultSettings", "int32s"],
    # LakeShore PID
    "Sample Heater Range": ["LSSampleRangePID", "int32s"],
    "Reset Defaults": ["ResetDefaultSettings", "int32s"],
    "PID Channel": ["LSPIDChannel", "int32s"],
    "PID P-value": ["LSPID_P", "floats"],
    "PID I-value": ["LSPID_I", "floats"],
    "PID D-value": ["LSPID_D", "floats"],
}
###########################################################################################
pressure_map = {
    "P1": "P1Bypass",
    "P2": "P2ScrollOut",
    "P3": "P3CompOut",
    "P4": "P4Return",
    "P5": "P5Dump",
    "P6": "P6OVC",
    "P7": "P7ScrollIn",
}
valid_pressure_keys = list(pressure_map.keys())
###########################################################################################
valid_channels = ["CH" + str(i) for i in range(16)]

valid_interlock_options = [
    "IL_Chamber",
    "IL_Cryo",
    "IL_S1",
    "IL_S2",
    "IL_S3",
    "IL_T1",
    "IL_T2",
    "IL_V1",
    "IL_V16",
    "IL_V17",
    "IL_V18",
    "IL_WUH",
]

bypass_map = {
    "T1": ["T1ServiceMode", "int32s"],
    "T2": ["T2ServiceMode", "int32s"],
    "S1": ["S1ServiceMode", "int32s"],
    "S2": ["S2ServiceMode", "int32s"],
    "V1": ["V1ServiceMode", "int32s"],
    "V16": ["V16ServiceMode", "int32s"],
    "V17": ["V17ServiceMode", "int32s"],
    "V18": ["V18ServiceMode", "int32s"],
    "Mix Recovery": ["MixRecoveryServiceMode", "int32s"],
    "Cryomech": ["CryoServiceMode", "int32s"],
    # "Heaters": ["WUHServiceMode", "int32s"], --> removed from the Logic
}
valid_bypass_options = bypass_map.keys()
###########################################################################################
###########################################################################################
# module, channel, logic_name
gghs_current_map = {
    "G1": [5, 4, "GGHS1Analog_"],
    "G2": [5, 5, "GGHS2Analog_"],
    "G3": [5, 6, "GGHS3Analog_"],
}

gghs_switch_map = {
    "G1": [4, 18, "GGHS1Switch_"],
    "G2": [4, 19, "GGHS2Switch_"],
    "G3": [4, 20, "GGHS3Switch_"],
}

if WIRING_V == "1":
    valve_map = {
        "V1": [4, 21, "V1TMPIn_"],
        "V2": [4, 14, "V2TMPOut_"],
        "V3": [4, 2, "V3CompBypass_"],
        "V4": [4, 1, "V4CompIn_"],
        "V5": [4, 0, "V5CompOut_"],
        "V6": [4, 7, "V6TrapBypass_"],
        "V7": [4, 6, "V7TrapIn_"],
        "V8": [4, 8, "V8TrapOut_"],
        "V9": [4, 13, "V9Return_"],
        "V10": [4, 11, "V10Dump_"],
        "V11": [4, 9, "V11DumpInject_"],
        "V12": [4, 4, "V12NeedleBlock_"],
        "V13": [4, 3, "V13NeedleBypass_"],
        "V14": [4, 12, "V14Bypass_"],
        "V15": [4, 10, "V15Collection_"],
        "V16": [4, 5, "V16Util_"],
        "V17": [4, 15, "V17UtilOVC_"],
        "V18": [4, 18, "V18UtilVent_"],
    }
elif WIRING_V == "2":
    valve_map = {
        "V1": [4, 21, "V1TMPIn_"],
        "V2": [4, 0, "V2TMPOut_"],
        "V3": [4, 1, "V3CompBypass_"],
        "V4": [4, 2, "V4CompIn_"],
        "V5": [4, 3, "V5CompOut_"],
        "V6": [4, 4, "V6TrapBypass_"],
        "V7": [4, 5, "V7TrapIn_"],
        "V8": [4, 6, "V8TrapOut_"],
        "V9": [4, 7, "V9Return_"],
        "V10": [4, 8, "V10Dump_"],
        "V11": [4, 9, "V11DumpInject_"],
        "V12": [4, 10, "V12NeedleBlock_"],
        "V13": [4, 11, "V13NeedleBypass_"],
        "V14": [4, 12, "V14Bypass_"],
        "V15": [4, 13, "V15Collection_"],
        "V16": [4, 14, "V16Util_"],
        "V17": [4, 15, "V17UtilOVC_"],
        "V18": [4, 16, "V18UtilVent_"],
    }
else:
    print("Make sure to set the correct version for the Wiring of the fridge.")

# These four valves are controlled differently compared to the rest of the valves.
exception_valves = {
    "V1": "V1TMPIn",
    "V16": "V16Util",
    "V17": "V17UtilOVC",
    "V18": "V18UtilVent",
}

compressor_map = {
    "S1": [3, 0, "S1Run_"],
    "S2": [3, 3, "S2Run_"],
    "S3": [3, 4, "S3Run_"],
}

turbo_map = {
    "T1": [3, 2, "T1Run_"],
    "T2": [3, 5, "T2Run_"],
}

gghs_options = list(gghs_switch_map.keys())
compressor_options = list(compressor_map.keys())
valve_options = list(valve_map.keys())
turbo_options = list(turbo_map.keys())

valid_maps_and_options = {
    "gghs_switch": (gghs_switch_map, gghs_options),
    "gghs_current": (gghs_current_map, gghs_options),
    "compressors": (compressor_map, compressor_options),
    "valves": (valve_map, valve_options),
    "turbos": (turbo_map, turbo_options),
}
valve_options = ["V" + str(i) for i in range(1, 19)]
###########################################################################################
all_valid_inputs = {
    "interlocks": valid_interlock_options,
    "gghs": gghs_current_map.keys(),
    "compressors": compressor_map.keys(),
    "pressures": valid_pressure_keys,
    "settings": settings_map.keys(),
    "bypass": bypass_map.keys(),
    "turbos": turbo_map.keys(),
    "temperatures": valid_channels,
    "resistances": valid_channels,
    "valves": valve_options,
}
###########################################################################################
