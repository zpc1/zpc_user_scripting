import sys
sys.path.append('../')

from fridge_info import all_valid_inputs
from env import *
import traceback
import requests
import ast

# ignore insecure https requests warning:
from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)
###########################################################################################
def get_url_from_plc():
    header = {
        'apiKey' : API_KEY,
        'Content-Type' : 'application/json'
    }
    BASE_URL = 'https://{}/pac/device/strategy'.format(PLC_IP_ADDRESS)
    return header, BASE_URL
###########################################################################################
def get_fridge_data(is_table, variable_type, variable_name, index='NA'):
    header, BASE_URL = get_url_from_plc()
    tables_vars = 'tables' if is_table else 'vars'


    url = '{}/{}/{}/{}'.format(BASE_URL, tables_vars, variable_type, variable_name)
    response = requests.get(url, headers=header, verify=False)
    return user_response(response, index)
###########################################################################################
def set_fridge_data(is_table, variable_type, variable_name, new_value, start_index=0):
    header, BASE_URL = get_url_from_plc()
    tables_vars = 'tables' if is_table else 'vars'
    payload = new_value if is_table else '{"value":%s}' % new_value

    try:
        if is_table:
            url = "{}/{}/{}/{}?startIndex={}".format(
                BASE_URL, tables_vars, variable_type, variable_name, start_index
            )
            response = requests.post(url, json=payload, headers=header, verify=False)
        else:
            url = "{}/{}/{}/{}".format(BASE_URL, tables_vars, variable_type, variable_name)
            if variable_type=="strings":
                response = requests.post(url, data=payload, headers=header, verify=False)
            else:
                response = requests.post(url, json=payload, headers=header, verify=False)

        return (None, "", ast.literal_eval(response.text).get("message"))
    
    except Exception as ex:
        error_backtrace = traceback.format_exc()
        if logging: print(error_backtrace)
        return None, "failed", error_backtrace

###########################################################################################
###########################################################################################
def user_response(response, index='NA'):
    if response.status_code != 200:
        return 'Read request failed (200).', 'failed', None
    
    else:
        try:
            # deal with tables and regular floats and ints
            return_val = ast.literal_eval(response.text)
            return_val = return_val[index] if index!='NA' else return_val
            return_val = return_val.get('value') if type(return_val)==dict else return_val
            return return_val, "successful", ''
        except Exception as ex:
            # error_backtrace = "".join(traceback.TracebackException.from_exception(ex).format())
            error_backtrace = traceback.format_exc()
            if logging: print(error_backtrace)
            return None, "failed", error_backtrace
###########################################################################################
def failed_return(message, option='', valid_options=''):
    if option and valid_options:
        message = "\n{} is not a valid option. Please choose from the following list:\n{}\n".format(option, valid_options)
    return None, 'failed', message
###########################################################################################
def get_all_available_inputs(device=''):
    if not device:
        return sorted(list(all_valid_inputs.keys()))
    elif device in all_valid_inputs:
        if device in ['resistances', 'temperatures', 'valves']:
            return all_valid_inputs[device]
        return sorted(all_valid_inputs[device])
    else:
        return '{} is not a valid device'.format(device)
###########################################################################################
