import sys
sys.path.append('../')

from env import logging
import traceback
import requests
import ast

from utils import get_fridge_data, set_fridge_data
from fridge_info import settings_map

# ignore insecure https requests warning:
from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

from fridge_info import valid_interlock_options, valid_channels
from fridge_info import bypass_map, valid_bypass_options
from fridge_info import valid_pressure_keys, pressure_map
from utils import user_response, failed_return

###########################################################################################
def get_temperature_or_resistance(table_name, channel):
    '''
    The input has to be a string.
    Valid options are "CH0", "CH2", ..., "CH15"
    '''

    if type(channel)!=str:
        return failed_return("The input has to be a string!")
    
    if channel not in valid_channels and channel!='NA':
        return failed_return('', channel, valid_channels)

    channel_id = int(channel.replace('CH','')) if channel!='NA' else channel
    return get_fridge_data(True, 'floats', table_name, channel_id)
############################################
def get_temperature(channel):
    return get_temperature_or_resistance('LSKFloats', channel)

def get_resistance(channel):
    return get_temperature_or_resistance('LsOhmFloats', channel)

# Using NA, we return the full list and don't index the list!
def get_all_temperatures():
    return get_temperature_or_resistance('LSKFloats', channel='NA')

def get_all_resistances():
    return get_temperature_or_resistance('LsOhmFloats', channel='NA')

###########################################################################################
def get_pressure(pressure_id):
    if pressure_id not in valid_pressure_keys:
        return failed_return('', pressure_id, valid_pressure_keys)
    pressure_key = pressure_map[pressure_id]
    return get_fridge_data(False, 'floats', pressure_key)
###########################################################################################
def get_flow():
    return get_fridge_data(False, 'floats', 'Flow_umol')
###########################################################################################
def get_interlock_status(channel):
    if channel not in valid_interlock_options:
        return failed_return('', channel, valid_interlock_options)
    
    return get_fridge_data(False, 'int32s', channel)
###########################################################################################
###########################################################################################
###########################################################################################
def set_get_settings_page_value(set_get_state, channel, value='None'):
    if channel not in settings_map.keys():
        return failed_return('', channel, list(settings_map.keys()))
    
    variable_name, variable_type = settings_map[channel]
    if set_get_state=='set':
        return user_response(set_fridge_data(False, variable_type, variable_name, new_value=value))
    elif set_get_state=='get':
        return get_fridge_data(False, variable_type, variable_name)
###########################################################################################
def set_get_bypass_status(set_get_state, channel, value='None'):
    if channel not in valid_bypass_options:
        return failed_return('', channel, valid_bypass_options)
    
    variable_name, variable_type = bypass_map[channel]
    if set_get_state=='set':
        return user_response(set_fridge_data(False, variable_type, variable_name, new_value=value))
    elif set_get_state=='get':
        return get_fridge_data(False, variable_type, variable_name)
###########################################################################################
def set_settings_page_value(channel, value):
    return set_get_settings_page_value('set', channel, value)

def get_settings_page_value(channel):
    return set_get_settings_page_value('get', channel)
###########################################################################################
def set_bypass_status(channel, value):
    value = int(value)
    if value not in[0,1]:
        return (None, 'failed', 'Value should be either 0 or 1!')
    return set_get_bypass_status('set', channel, value)

def get_bypass_status(channel):
    return set_get_bypass_status('get', channel)
###########################################################################################
def set_lakeshore_pid_kelvin_target(value):
    return user_response(set_fridge_data(False, "floats", "LSPIDKelvinSP", new_value=value))
def get_lakeshore_pid_kelvin_target():
    return get_fridge_data(False, "floats", "LSPIDKelvinSP")
###########################################################################################
def get_external_trap_status():
    return get_fridge_data(False, 'int32s', 'ExtTrapRun')
###########################################################################################
