import sys
sys.path.append('../')

import traceback
import optommp

from utils import set_fridge_data, user_response, failed_return
from fridge_info import exception_valves, valid_maps_and_options
from interlocks import handle_manual_interlocks
from env import logging, PLC_IP_ADDRESS

###########################################################################################
def handle_exception_valves(channel, state):
    return user_response(set_fridge_data(False, 'int32s', exception_valves[channel], state))
####################################################################################
def check_if_option_vallid(channel, device):
    try:
        valid_map, valid_options = valid_maps_and_options[device]
        if channel not in valid_options:
            return True, failed_return('', channel, valid_options)
        return False, valid_map
    except Exception as ex:
        if logging:
            print (traceback.format_exc())
        return True, failed_return(traceback.format_exc())
####################################################################################
def control_fridge(channel, device, set_get_state, state, set_value='None'):

    # Handle UI interlocks that can bypass the logic through HTTP request
    if set_get_state=='set':
        message = handle_manual_interlocks(channel)
        if message!='NA':
            return (None, message, 'failed')
        
    return_value_status = "successful"
    try:
        if device=='valves' and set_get_state=='set' and channel in exception_valves:
            return handle_exception_valves(channel, state)
        
        failed, valid_map = check_if_option_vallid(channel, device)
        if failed:
            failed_response = valid_map # this is actually the full return
            return failed_response

        module, channel, _ = valid_map[channel]
        grvEpic = optommp.O22MMP(PLC_IP_ADDRESS)
        # hierarchy:
        # 1. handle getting the gghs current value
        # 2. setting an actual value of a channel
        # 3. setting the state of a channel: open=1, close=0
        # 4. getting the state of a channel: open=1, close=0
        if device=='gghs_current' and set_get_state=='get': return_value_status = grvEpic.GetAnalogPointValue(module, channel)
        elif set_get_state=='set' and set_value!='None': grvEpic.SetAnalogPointValue (module, channel, set_value)
        elif set_get_state=='set': grvEpic.SetDigitalPointState(module, channel, state)
        elif set_get_state=='get': return_value_status = grvEpic.GetDigitalPointState(module, channel)
        grvEpic.close()
        
        return (return_value_status, "successful", '') if set_get_state=='get' else (None, 'successful', '')
    
    except Exception as ex:
        # error_backtrace = "".join(traceback.TracebackException.from_exception(ex).format())
        if logging: print (traceback.format_exc())
        return failed_return(traceback.format_exc())
####################################################################################
def control_trubo_compressor(channel, device, state):
    '''
    compressor: "S1", "S2", and "S3"
    turbo: "T1" and "T2"
    '''
    failed, valid_map = check_if_option_vallid(channel, device)
    if failed:
        failed_response = valid_map  # this is actually the full return
        return failed_response

    variable_name = channel+'Run'# e.g.T2Run
    return user_response(set_fridge_data(False, 'int32s', variable_name, state))
####################################################################################
####################################################################################
def open_gghs_switch(channel):
    return control_fridge(channel, 'gghs_switch', set_get_state='set', state=1)

def close_gghs_switch(channel):
    return control_fridge(channel, 'gghs_switch', set_get_state='set', state=0)

def get_gghs_switch_status(channel):
    return control_fridge(channel, 'gghs_switch', set_get_state='get', state=None)

def get_gghs_current_value(channel):
    return control_fridge(channel, 'gghs_current', set_get_state='get', state=None)

def set_gghs_current_value(channel, set_value):
    set_value = 10 if set_value>10 else set_value
    return control_fridge(channel, 'gghs_current', set_get_state='set', state=None, set_value=set_value)
####################################################################################
def open_valve(channel):
    return control_fridge(channel, 'valves', set_get_state='set', state=1)

def close_valve(channel):
    return control_fridge(channel, 'valves', set_get_state='set', state=0)
####################################################################################
def get_valve_status(channel):
    return control_fridge(channel, 'valves', set_get_state='get', state=None)

def get_turbo_status(channel):
    return control_fridge(channel, 'turbos', set_get_state='get', state=None)

def get_compressor_status(channel):
    return control_fridge(channel, 'compressors', set_get_state='get', state=None)
####################################################################################
def turn_on_turbo(channel):
    return control_trubo_compressor(channel, device='turbos', state=1)

def turn_off_turbo(channel):
    return control_trubo_compressor(channel, device='turbos', state=0)

def turn_on_compressor(channel):
    return control_trubo_compressor(channel, device='compressors', state=1)

def turn_off_compressor(channel):
    return control_trubo_compressor(channel, device='compressors', state=0)
####################################################################################
####################################################################################
# set_fridge_data returns the status of the rquest;
# since it's a setter funbction, value is always None
def start_warmup_heater():
    return user_response(set_fridge_data(False, 'int32s', 'WUH', 1))
def stop_warmup_heater():
    return user_response(set_fridge_data(False, 'int32s', 'WUH', 0))

def start_collect_mix():
    return user_response(set_fridge_data(False, 'int32s', 'RecoveringMix', 1))
def stop_collect_mix():
    return user_response(set_fridge_data(False, 'int32s', 'RecoveringMix', 0))

def start_condense_mix():
    return user_response(set_fridge_data(False, 'int32s', 'CondensingMix', 1))
def stop_condense_mix():
    return user_response(set_fridge_data(False, 'int32s', 'CondensingMix', 0))

def start_pulse_tube():
    return user_response(set_fridge_data(False, 'int32s', 'CryoRun', 1))
def stop_pulse_tube():
    return user_response(set_fridge_data(False, 'int32s', 'CryoRun', 0))

def start_pulsing():
    return user_response(set_fridge_data(False, "int32s", "ACDP_start", 1))
def stop_pulsing():
    return user_response(set_fridge_data(False, "int32s", "ACDP_start", 0))

def start_flush_and_pump():
    return user_response(set_fridge_data(False, "int32s", "FaP_start", 1))
def stop_flush_and_pump():
    return user_response(set_fridge_data(False, "int32s", "FaP_start", 0))

def start_impedence_test():
    return user_response(set_fridge_data(False, "int32s", "Imp_start", 1))
def stop_impedence_test():
    return user_response(set_fridge_data(False, "int32s", "Imp_start", 0))

def open_external_trap():
    return user_response(set_fridge_data(False, 'int32s', 'ExtTrapRun', 1))
def close_external_trap():
    return user_response(set_fridge_data(False, 'int32s', 'ExtTrapRun', 0))


def stop_auto_cooldown():
    return user_response(set_fridge_data(False, "int32s", "ACD_start", 0))

def start_auto_cooldown():
    return user_response(set_fridge_data(False, "int32s", "ACD_start", 1))

def start_auto_cooldown_stage2():
    return user_response(set_fridge_data(False, "int32s", "ACD_stage2", 1))
    # The user has to call "ACD_start" themselves
    # _, status, error = user_response(set_fridge_data(False, "int32s", "ACD_stage2", 1))
    # if status=="successful":
    #     return user_response(set_fridge_data(False, "int32s", "ACD_start", 1))
    # return None, "failed", error
def start_auto_cooldown_stage3():
    return user_response(set_fridge_data(False, "int32s", "ACD_stage3", 1))
def start_auto_cooldown_stage4():
    return user_response(set_fridge_data(False, "int32s", "ACD_stage4", 1))
def start_auto_cooldown_stage5():
    return user_response(set_fridge_data(False, "int32s", "ACD_stage5", 1))

def stop_auto_cooldown_stage2():
    return user_response(set_fridge_data(False, "int32s", "ACD_stage2", 0))
def stop_auto_cooldown_stage3():
    return user_response(set_fridge_data(False, "int32s", "ACD_stage3", 0))
def stop_auto_cooldown_stage4():
    return user_response(set_fridge_data(False, "int32s", "ACD_stage4", 0))
def stop_auto_cooldown_stage5():
    return user_response(set_fridge_data(False, "int32s", "ACD_stage5", 0))

def start_auto_warmup():
    return user_response(set_fridge_data(False, "int32s", "AWU_start", 1))
def stop_auto_warmup():
    return user_response(set_fridge_data(False, "int32s", "AWU_start", 0))

def start_lakeshore_pid_control():
    return user_response(set_fridge_data(False, "int32s", "LSPIDOn", 1))
def stop_lakeshore_pid_control():
    return user_response(set_fridge_data(False, "int32s", "LSPIDOn", 0))


####################################################################################
####################################################################################
