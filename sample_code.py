# we have prevented generating .pyc files. You can comment this if you wish!
import sys
sys.dont_write_bytecode = True

from functions import *


print(get_all_available_inputs())
# ['bypass', 'compressors', 'gghs', 'interlocks', 'pressures', 'resistances', 'settings', 'temperatures', 'turbos', 'valves']

for item in get_all_available_inputs():
	print ('valid options for {}:'.format(item), get_all_available_inputs(item))
# valid options for bypass: ['Cryo', 'Mix', 'S1', 'S2', 'T1', 'T2', 'V1', 'V16', 'V17', 'V18', 'WUH']
# valid options for compressors: ['S1', 'S2', 'S3']
# valid options for gghs: ['G1', 'G2', 'G3']
# valid options for interlocks: ['IL_Chamber', 'IL_Cryo', 'IL_S1', 'IL_S2', 'IL_S3', 'IL_T1', 'IL_T2', 'IL_V1', 'IL_V16', 'IL_V17', 'IL_V18', 'IL_WUH']
# valid options for pressures: ['P1', 'P2', 'P3', 'P4', 'P5', 'P6', 'P7']
# valid options for resistances: ['CH0', 'CH1', 'CH2', 'CH3', 'CH4', 'CH5', 'CH6', 'CH7', 'CH8', 'CH9', 'CH10', 'CH11', 'CH12', 'CH13', 'CH14', 'CH15']
# valid options for settings: ['372 Scan Delay', 'Condense Start', 'Cooling Power Delay', 'Dump Empty', 'Dump Full Margin', 'Dump Full Setpoint', 'Dump Low', 'PLC Scan Delay', 'Reset Defaults', 'Roughing Max Time', 'Roughing Setpoint', 'S1 Cycle High', 'S1 Cycle Low', 'Switching Delay', 'T1 Scan Delay', 'T1 Start', 'T2 Ramp-Down', 'T2 Scan Time', 'Vac. Max Time', 'Vac. Setpoint']
# valid options for temperatures: ['CH0', 'CH1', 'CH2', 'CH3', 'CH4', 'CH5', 'CH6', 'CH7', 'CH8', 'CH9', 'CH10', 'CH11', 'CH12', 'CH13', 'CH14', 'CH15']
# valid options for turbos: ['T1', 'T2']
# valid options for valves: ['V1', 'V2', 'V3', 'V4', 'V5', 'V6', 'V7', 'V8', 'V9', 'V10', 'V11', 'V12', 'V13', 'V14', 'V15', 'V16', 'V17', 'V18']

# ===========================================================================
# ===========================================================================
print(get_temperature('CH1'))
# (22.0251, 'successful', '')
print(get_resistance('CH1'))
# (95.459, 'successful', '')

print(get_pressure('P1'))
# (0.0019079446, 'successful', '')
print(get_flow())
# (7.614401, 'successful', '')


print(get_all_temperatures())
# ([51.2503, 22.0251, 135.208, 0.0, 0.0, 0.0, 94.6377, 0.0, 0.0, 0.0, 146.891, 0.0, 0.0, 131.391, 0.0, 0.0], 'successful', '')
print(get_all_resistances())
# ([63.312, 95.459, 38.2894, 0.0, 1070.0, 1054.6, 107.746, 0.0, 1040.48, 0.0, 45.9243, 0.0, 1055.57, 38.9262, 2076.93, 0.0], 'successful', '')
# ===========================================================================
# Most of the following functions return: (None, '', 'successful'); unless a request is 'failed'
# exceptions are the "get status" functions that return 0 or 1 instead of None
# WARNING: be careful testing these functions:
# These functions will change the physical status of the fridge; e.g. opening or closing a valve.

# print(close_valve('V3'))

# status_value, status, message = get_valve_status('V3')
# _, status, message = close_valve('V3')
# _, status, message = open_valve('V3')

# status_value, status, message = get_compressor_status('S1)
# _, status, message = turn_off_compressor('S1')
# _, status, message = turn_on_compressor('S1')

# status_value, status, message = get_turbo_status('T1')
# _, status, message = turn_off_turbo('T1')
# _, status, message = turn_on_turbo('T1')
# ===========================================================================
# For the Gas Gap Handleing System (GGHS)

# status_value, status, message = get_gghs_switch_status('G1')
# _, status, message = open_gghs_switch('G1')
# _, status, message = close_gghs_switch('G1')

# current_value, status, message = get_gghs_current_value('G1')
# _, status, message = open_gghs_switch('G1')
# _, status, message = set_gghs_current_value('G1')
# ===========================================================================
# Settings Page

# settings_value, status, message = get_settings_page_value('Condense Start')
# _, status, message = set_settings_page_value('Condense Start', 6)

# in order to all setting variables to default
# _, status, message = set_settings_page_value('Reset Defaults', 1)
# ===========================================================================
# Interlocks and Bypasses

# status_value, status, message = get_interlock_status('IL_Chamber')
# status_value, status, message = get_bypass_status('Cryo')
# _, status, message = set_bypass_status('Cryo')
# ===========================================================================
# External Trap

# status_value, status, message = get_external_trap_status()
# _, status, message = open_external_trap()
# _, status, message = close_external_trap()
# ===========================================================================
# Operation Page

# _, status, message = stop_condense_mix()
# _, status, message = start_condense_mix()

# _, status, message = stop_collect_mix()
# _, status, message = start_collect_mix()

# _, status, message = stop_pulse_tube()
# _, status, message = start_pulse_tube()

# _, status, message = stop_warmup_heater()
# _, status, message = start_warmup_heater()
# ===========================================================================
# import time
# open_valve('V17')
# time.sleep(5)
# close_valve('V17')


