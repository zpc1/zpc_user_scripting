# Required Packages

There are only two packages you need to run any script you have for controlling the fridge which are included in the requirements.txt file. But feel free to install any other package you want.

# **NOTE**:
Please make sure to read the User Manual before you start scripting. Also, make sure to make the proper change for the **optommp** package, as described in the manual.